//
//  StreamConstants.swift
//  NetServiceStreaming
//
//  Created by Shakir Ali on 12/01/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import Foundation

struct StreamConstant {
    static let BufferLength = 512
}