//
//  Reader.swift
//  NetServiceStreaming
//
//  Created by Shakir Ali on 11/01/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import Foundation

protocol StreamReaderDelegate: class {
    func streamReader(StreamReader: StreamReader, didStartReadingDataOfLength length: Int)
    func streamReader(streamReader: StreamReader, didFinishReadingData data: NSData)
    func streamReader(streamReader: StreamReader, hadErrorReadingHeader error: NSError?)
    func streamReader(streamReader: StreamReader, hadErrorReadingBody error: NSError?)
}

enum StreamReaderState {
    case ReadingNotStarted
    case ReadingHeader
    case ReadingBody
    case ReadingCompleted
    case ReadingError
}

class StreamReader: NSObject {
    
    private(set) var inputStream: NSInputStream
    private(set) var state: StreamReaderState = .ReadingNotStarted
    private var totalBytesExpected = 0
    private lazy var bodyData = NSMutableData()
    private(set) weak var delegate: StreamReaderDelegate?
    private var totalBytesRead = 0
    
    
    init(stream: NSInputStream, delegate: StreamReaderDelegate) {
        self.inputStream = stream
        self.delegate = delegate
    }
    
    func readStream() {
        switch (state) {
            case .ReadingNotStarted:
                //First time read has called so switch the state to ReadingHeader.
                state = .ReadingHeader
                //Now read header.
                fallthrough
            case .ReadingHeader:
                self.processHeader()
                self.state = .ReadingBody
            case .ReadingBody:
                self.processBody()
            case .ReadingCompleted:
                print("Nothing to read as it is completed.")
            default:
                print("default")
        }
    }
    
    func reset() {
        self.state = .ReadingNotStarted
        self.totalBytesExpected = 0
        self.totalBytesRead = 0
        self.bodyData = NSMutableData()
    }
    
    private func processHeader() {
        let headerData = NSMutableData()
        var buf = [UInt8](count: sizeof(Int), repeatedValue: 0)
        let bytesRead = self.inputStream.read(&buf, maxLength: sizeof(Int))
        
        //error reading buffer
        if bytesRead < 0  {
            //error reading header.
            self.state = .ReadingError
            self.delegate?.streamReader(self, hadErrorReadingHeader: self.inputStream.streamError)
            return
        }
        
        //no data found in buffer so reset the stream reader.
        if (bytesRead == 0) {
            self.reset()
            return
        }
        
        //some data has been found.
        headerData.appendBytes(buf, length: bytesRead)
        headerData.getBytes(&self.totalBytesExpected, length: sizeof(Int))
        //header has been read successfully so switch the state to reading body.
        self.state = .ReadingBody
        self.delegate?.streamReader(self, didStartReadingDataOfLength:self.totalBytesExpected)
    }
    
    private func processBody() {
        var buf = [UInt8](count: StreamConstant.BufferLength, repeatedValue: 0)
        let bytesRead = inputStream.read(&buf, maxLength: StreamConstant.BufferLength)
        
        //error
        if bytesRead < 0 {
            self.state = .ReadingError
            self.delegate?.streamReader(self, hadErrorReadingBody: self.inputStream.streamError)
            return
        }
        
        //if received some data then append it.
        if bytesRead > 0 {
            self.bodyData.appendBytes(buf, length: bytesRead)
            self.totalBytesRead += bytesRead
        }
        
        if self.totalBytesRead == self.totalBytesExpected {
            self.state = .ReadingCompleted
            self.delegate?.streamReader(self, didFinishReadingData: self.bodyData)
        }
    }
}