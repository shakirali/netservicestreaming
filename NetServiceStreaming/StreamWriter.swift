//
//  StreamWriter.swift
//  NetServiceStreaming
//
//  Created by Shakir Ali on 11/01/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import Foundation

enum StreamProcessingState {
    case NotStarted
    case ProcessingHeader
    case ProcessingBody
    case Completed
    case Error
}

protocol StreamWriterDelegate: class {
    func streamWriterDidStartWritingData()
    func streamWriterDidFinishWritingData()
    func streamWriter(streamWriter: StreamWriter, hadErrorWritingHeader error: NSError?)
    func streamWriterHadFullCapacityHeaderError()
    func streamWriter(streamWriter: StreamWriter, hadErrorWritingBody error: NSError?)
    func streamWriterHadFullCapacityBodyError()
    func streamWriterMissingDataError()
}

class StreamWriter: NSObject {
    private(set) var processingState: StreamProcessingState = .NotStarted
    private(set) var outputStream: NSOutputStream
    private(set) var data: NSData?
    weak private(set) var delegate: StreamWriterDelegate?
    private var currentByteIndex: Int = 0
    
    init(stream: NSOutputStream, delegate: StreamWriterDelegate) {
        self.delegate = delegate
        self.outputStream = stream
    }
    
    func writeData(data: NSData) {
        self.data = data
        switch(self.processingState) {
        case .NotStarted:
            self.processingState = .ProcessingHeader
            fallthrough
        case .ProcessingHeader:
            self.writeHeader()
        case .ProcessingBody:
            self.writeBody()
        case .Completed:
            print("Status is completed. so no new data to write.")
        default: //need to implement
            print("default")
        }
    }
    
    func reset() {
        self.processingState = .NotStarted
        self.data = nil
        self.currentByteIndex = 0
    }
    
    private func writeHeader() {
        guard let data = self.data else {
            self.processingState = .Error
            self.delegate?.streamWriterMissingDataError()
            return
        }
        
        var headerContent = data.length
        let headerData = NSData(bytes: &headerContent, length: sizeof(Int))
        let headerPtr = UnsafePointer<UInt8>(headerData.bytes)
        var buf = [UInt8](count: sizeof(Int), repeatedValue: 0)
        memcpy(&buf, headerPtr, sizeof(Int))
        let bytesWritten = outputStream.write(&buf, maxLength: buf.count)
        
        if (bytesWritten == -1){
            self.processingState = .Error
            self.delegate?.streamWriter(self, hadErrorWritingHeader: self.outputStream.streamError)
            return
        }
        
        //Could not write header so reset the writer.
        if (bytesWritten == 0) {
            self.reset()
            self.delegate?.streamWriterHadFullCapacityHeaderError()
            return
        }

        //header was successfully written to stream.
        self.processingState = .ProcessingBody
        self.delegate?.streamWriterDidStartWritingData()
    }
    
    private func writeBody() {
        guard let data = self.data else {
            self.processingState = .Error
            self.delegate?.streamWriterMissingDataError()
            return
        }
        
        if self.currentByteIndex < data.length {
            let outputBytesReadPtr = UnsafePointer<UInt8>(data.bytes) + self.currentByteIndex
            let bytesToWrite = (data.length - self.currentByteIndex >= StreamConstant.BufferLength) ? StreamConstant.BufferLength : (data.length - self.currentByteIndex)
            
            var buf = [UInt8](count: StreamConstant.BufferLength, repeatedValue: 0)
            
            memcpy(&buf, outputBytesReadPtr, bytesToWrite)
            
            let bytesWritten = outputStream.write(buf, maxLength: bytesToWrite)
            
            //error in writing to stream.
            if bytesWritten == -1 {
                self.processingState = .Error
                self.delegate?.streamWriter(self, hadErrorWritingBody: self.outputStream.streamError)
                return
            }
            
            if bytesWritten == 0 {
                self.delegate?.streamWriterHadFullCapacityBodyError()
                return
            }
            
            //bytes successfully written.
            self.currentByteIndex += bytesWritten
        }
        
        //data is completely written.
        if self.currentByteIndex == data.length {
            self.processingState = .Completed
            self.delegate?.streamWriterDidFinishWritingData()
        }
    }
}