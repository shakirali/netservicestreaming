//
//  ViewController.swift
//  NetServiceStreaming
//
//  Created by Shakir Ali on 04/01/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import UIKit

protocol NetServiceSettable {
    var serviceType: String! { get set }
    var domain: String! { get set }
}

class ServiceViewController: UIViewController, NSNetServiceDelegate, NetServiceSettable, ServiceBrowserDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, DataTransmittorDelegate {
    var advertisedNetService: NSNetService?
    var serviceType: String! = "_musicstreaming._tcp."
    var domain: String! = "local."
    var currentImageData: NSData?
    var dataTransmittor: DataTransmittor?
    var isServiceRunning = false
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var photoBtn: UIButton!
    @IBOutlet weak var inviteBtn: UIBarButtonItem!
    @IBOutlet weak var serviceStatusLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.photoBtn.enabled = false
        self.serviceStatusLabel.text = "Not Connected"
    }

    //Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        if segue.identifier == "DisplayServiceBrowserController" {
            if var destinationViewController = segue.destinationViewController as? NetServiceSettable {
                destinationViewController.serviceType = self.serviceType
                destinationViewController.domain = self.domain
            }
            if let destinationViewController = segue.destinationViewController as? ServiceBrowserViewController {
                destinationViewController.delegate = self
            }
        }
    }
    
    //MARK: Actions
    @IBAction func inviteBtnTapped(sender: UIBarButtonItem) {
        self.advertisedNetService = NSNetService(domain: self.domain, type: self.serviceType, name: UIDevice.currentDevice().name, port: 0)
        guard let netService = self.advertisedNetService else {
            self.displayAlertMessage("Problem in creating the invitation")
            return
        }
        netService.includesPeerToPeer = true
        netService.delegate = self
        netService.publishWithOptions(.ListenForConnections)
    }
    
    @IBAction func photoButtonTapped(sender: UIButton) {
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.Camera) {
            imagePicker.sourceType = .Camera
        }else if UIImagePickerController.isSourceTypeAvailable(.PhotoLibrary) {
            imagePicker.sourceType = .PhotoLibrary
        }else if UIImagePickerController.isSourceTypeAvailable(.SavedPhotosAlbum) {
            imagePicker.sourceType = .SavedPhotosAlbum
        }
        imagePicker.delegate = self
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: NSNetServiceDelegate
    func netServiceDidPublish(sender: NSNetService) {
        print("netServiceDidPublish")
        self.isServiceRunning = true
        self.inviteBtn.enabled = false
        self.serviceStatusLabel.text = "Published invitation. Waiting for someone to join."
    }
    
    func netService(sender: NSNetService, didNotPublish errorDict: [String : NSNumber]) {
        print("netServiceDidNotPublish")
        self.isServiceRunning = false
        self.inviteBtn.enabled = true
        self.serviceStatusLabel.text = "Invitation has not been successfull. Please invite again."
    }
    
    func netServiceWillResolve(sender: NSNetService) {
        print("netServiceWillResolve")
    }
    
    func netService(sender: NSNetService, didNotResolve errorDict: [String : NSNumber]) {
        self.isServiceRunning = false
        self.inviteBtn.enabled = true
        self.serviceStatusLabel.text = "Invitation has not been successfull. Please invite again."
    }
    
    func netServiceDidResolveAddress(sender: NSNetService) {
        print("DidResolveAddress")
        
        var inputStream: NSInputStream?
        var outputStream: NSOutputStream?
        
        assert(self.advertisedNetService != nil)
        
        guard let service = self.advertisedNetService else { return }
        
        if (self.dataTransmittor == .None) {
            let success = service.getInputStream(&inputStream, outputStream: &outputStream)
            
            if (!success) {
                self.resetService()
            }
            else {
                if let inStream = inputStream, outStream = outputStream {
                    self.dataTransmittor = DataTransmittor(inputStream: inStream, outputStream: outStream, delegate: self)
                }
            }
        }
    }
    
    
    func netServiceDidStop(sender: NSNetService) {
        print("netServiceDidStop")
        self.isServiceRunning = false
        self.inviteBtn.enabled = true
    }
    
    func netService(sender: NSNetService, didAcceptConnectionWithInputStream inputStream: NSInputStream, outputStream: NSOutputStream) {
        // Due to a bug <rdar://problem/15626440>, this method is called on some unspecified
        // queue rather than the queue associated with the net service (which in this case
        // is the main queue).  Work around this by bouncing to the main queue.
        NSOperationQueue.mainQueue().addOperationWithBlock {
            print("didAcceptConnectionWithInputStream")
            // should either have both or neither
            //assert((self.inputStream != nil) == (self.outputStream != nil))
            
            //New connection.
            if (self.dataTransmittor == .None) {
                self.advertisedNetService?.stop()
                self.dataTransmittor = DataTransmittor(inputStream: inputStream, outputStream: outputStream, delegate: self)
            } else {
                //reject the current request.
                inputStream.open()
                inputStream.close()
                outputStream.open()
                outputStream.close()
            }
            
        }
    }
    
    //MARK: MusicNetServiceBrowserViewController
    func serviceBrowserViewController(browser: ServiceBrowserViewController, didSelectSerivce service:NSNetService) {
        self.advertisedNetService = service
        self.advertisedNetService?.delegate = self
        self.advertisedNetService?.resolveWithTimeout(0)
    }
    
    func resetService() {
        self.dataTransmittor = nil
        if (!self.isServiceRunning) {
            self.advertisedNetService?.publishWithOptions(.ListenForConnections)
        }
    }
    
    //MARK: UIImagePickerController
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        guard let photo = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            picker .dismissViewControllerAnimated(true, completion: nil)
            return
        }
        
        self.imageView.image = photo
        
        picker.dismissViewControllerAnimated(true, completion: nil)
        
        let image = photo.scaleToSize(CGSizeMake(300.0, 300.0))
        guard let jpeg = UIImageJPEGRepresentation(image, 0.0) else { return }
        self.currentImageData = jpeg
        guard let transmittor = self.dataTransmittor  else {
            displayAlertMessage("Service connections are closed.")
            return
        }
        transmittor.transmitData(self.currentImageData!)
    }
    
    //MARK: DataTransmittorDelegate
    func dataTransmittorDidOpenStreams() {
        self.photoBtn.enabled = true
        self.serviceStatusLabel.text = "Connected with another device."
    }
    
    func dataTransmittor(dataTransmittor: DataTransmittor, receivedError: NSError?) {
        self.dataTransmittor = nil
        self.advertisedNetService = nil
        self.photoBtn.enabled = false
        if let error = receivedError {
            displayAlertMessage(String(format: "Connection is closed with error %@", arguments: [error.localizedDescription]))
        } else {
            displayAlertMessage("Connection is closed")
        }
        self.serviceStatusLabel.text = "Connection has ended."
    }
    
    func dataTransmittorDidStartReadingData() {
        self.imageView.image = nil
        self.activityIndicatorView.startAnimating()
        self.serviceStatusLabel.text = "Receiving photo."
    }
    
    func dataTransmittorDidFinishReadingData(data: NSData) {
        self.resetUI()
        self.imageWithData(data)
        self.serviceStatusLabel.text = "Photo received."
    }
    
    func dataTransmittorDidStartWritingData() {
        self.activityIndicatorView.startAnimating()
        self.serviceStatusLabel.text = "Sending photo"
    }
    
    func dataTransmittorDidFinishWritingData() {
        self.resetUI()
        self.serviceStatusLabel.text = "Photo sent"
    }
    
    func dataTransmittorEnded() {
        self.dataTransmittor = nil
        self.advertisedNetService = nil
        self.resetUI()
        displayAlertMessage("Connection with the other device is closed.")
        self.serviceStatusLabel.text = "Connection has ended"
        self.inviteBtn.enabled = true
    }

    //MARK: UIAlertView
    func displayAlertMessage(message: String) {
        let alertController = UIAlertController(title: "NetService", message: message, preferredStyle: .Alert)

        let OKAction = UIAlertAction(title: "OK", style: .Default, handler: { action in alertController.dismissViewControllerAnimated(true, completion: nil)})
        alertController.addAction(OKAction)

        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    //MARK: Helper
    func imageWithData(data: NSData) {
        if let image = UIImage(data: data) {
            self.imageView.image = image
            self.imageView.contentMode = .ScaleAspectFill
        }
    }
    
    func resetUI() {
        self.activityIndicatorView.stopAnimating()
    }
}

