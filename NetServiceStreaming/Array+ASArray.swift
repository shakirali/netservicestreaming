//
//  Array+ASArray.swift
//  NetServiceStreaming
//
//  Created by Shakir Ali on 24/01/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    // Remove first collection element that is equal to the given `object`:
    mutating func removeObject(object : Generator.Element) {
        if let index = self.indexOf(object) {
            self.removeAtIndex(index)
        }
    }
}
