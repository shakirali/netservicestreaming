//
//  ServiceBrowserViewController.swift
//  NetServiceStreaming
//
//  Created by Shakir Ali on 04/01/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import UIKit

protocol ServiceBrowserDelegate: class {
    func serviceBrowserViewController(browser: ServiceBrowserViewController, didSelectSerivce service:NSNetService)
}

class ServiceBrowserViewController: UITableViewController, NSNetServiceBrowserDelegate, NetServiceSettable {

    var services = Array<NSNetService>()
    var searching = false
    var serviceType: String!
    var netServiceBrowser: NSNetServiceBrowser!
    var domain: String!
    weak var delegate: ServiceBrowserDelegate?
    private var currentContext = 0
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.netServiceBrowser = NSNetServiceBrowser()
        self.netServiceBrowser.delegate = self
        self.netServiceBrowser.includesPeerToPeer = true
        self.netServiceBrowser.searchForServicesOfType(self.serviceType, inDomain: self.domain)
        self.addObserver(self, forKeyPath: "searching", options: .New, context: &self.currentContext)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        self.netServiceBrowser.stop()
        self.removeObserver(self, forKeyPath: "searching", context: &self.currentContext)
    }
    
    //MARK: Helper
    func updateUI() {
        self.tableView.reloadData()
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.services.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("serviceCell", forIndexPath: indexPath)
         cell.textLabel?.text = self.services[indexPath.row].name
        return cell
    }
    
    //MARK: TableViewDelegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let service = self.services[indexPath.row]
        self.delegate?.serviceBrowserViewController(self, didSelectSerivce: service)
        self.navigationController?.popViewControllerAnimated(true)
    }

    //MARK: NSBrowserDelegate
    func netServiceBrowserWillSearch(browser: NSNetServiceBrowser) {
        self.searching = true
        print("netServiceBrowserWillSearch")
    }
    
    func netServiceBrowserDidStopSearch(browser: NSNetServiceBrowser) {
        self.searching = false
        print("netServiceBrowserDidStopSearch")
    }
    
    func netServiceBrowser(browser: NSNetServiceBrowser, didNotSearch errorDict: [String : NSNumber]) {
        self.searching = false
        self.displayMessage("Unable to search for the service")
    }
    
    func netServiceBrowser(browser: NSNetServiceBrowser, didFindService service: NSNetService, moreComing: Bool) {
        self.services.append(service)
        if (!moreComing) {
            self.tableView!.reloadData()
        }
    }
    
    func netServiceBrowser(browser: NSNetServiceBrowser, didRemoveService service: NSNetService, moreComing: Bool) {
        self.services.removeObject(service)
        if (!moreComing) {
            self.updateUI()
        }
    }
    
    //MARK: UIAlertView
    func displayMessage(message: String) {
        
        let alertController = UIAlertController(title: "NetService", message: message, preferredStyle: .Alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .Default, handler: { action in alertController.dismissViewControllerAnimated(true, completion: nil)})
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
        print(message)
    }
    
    //KVO
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        
        guard let keyPath = keyPath else { return }
        
        if context == &self.currentContext {
            if let newValue = change?[NSKeyValueChangeNewKey] as? Bool where keyPath == "searching"{
                self.updateSearchUI(newValue)
            }
        } else {
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        }
    }
    
    func updateSearchUI(searchValue: Bool) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = searchValue
    }
}