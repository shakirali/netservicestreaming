//
//  StreamManager.swift
//  NetServiceStreaming
//
//  Created by Shakir Ali on 12/01/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import Foundation

protocol DataTransmittorDelegate: class {
    func dataTransmittorDidStartWritingData()
    func dataTransmittorDidFinishWritingData()
    func dataTransmittorDidStartReadingData()
    func dataTransmittorDidFinishReadingData(data: NSData)
    
    func dataTransmittorDidOpenStreams()
    func dataTransmittor(dataTransmittor: DataTransmittor, receivedError: NSError?)
    func dataTransmittorEnded()
}

class DataTransmittor : NSObject, NSStreamDelegate, StreamReaderDelegate, StreamWriterDelegate {
    
    var inputStream: NSInputStream
    var outputStream: NSOutputStream
    var streamOpenCount = 0
    var delegate: DataTransmittorDelegate?
    var streamReader: StreamReader?
    var streamWriter: StreamWriter?
    var data: NSData?
    let useSSL: Bool = false
    
    init(inputStream: NSInputStream, outputStream: NSOutputStream, delegate: DataTransmittorDelegate) {
        self.inputStream = inputStream
        self.outputStream = outputStream
        self.delegate = delegate
        super.init()
        self.openStreams()
    }
    
    deinit {
        self.closeStreams()
    }
    
    func transmitData(data: NSData) {
        assert(self.streamOpenCount == 2)
        
        self.data = data
        //reset writer before use.
        self.streamWriter?.reset()
        
        if (self.outputStream.hasSpaceAvailable == true) {
            streamWriter?.writeData(data)
        }
    }
    
    func openStreams() {
        assert(self.streamOpenCount == 0)
        
        self.inputStream.delegate = self
        self.inputStream.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        if (self.inputStream.streamStatus == .NotOpen) {
            self.inputStream.open()
        }
        
        self.outputStream.delegate = self
        self.outputStream.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        if (self.outputStream.streamStatus == .NotOpen) {
            self.outputStream.open()
        }
        
        if self.useSSL {
            self.inputStream.setProperty(NSStreamSocketSecurityLevelNegotiatedSSL, forKey: NSStreamSocketSecurityLevelKey)
            self.outputStream.setProperty(NSStreamSocketSecurityLevelNegotiatedSSL, forKey: NSStreamSocketSecurityLevelKey)
            
            let settings: [String:AnyObject] = [kCFStreamSSLValidatesCertificateChain as String : NSNumber(bool: true)]
            CFReadStreamSetProperty(self.inputStream, kCFStreamPropertySSLSettings, settings as CFTypeRef)
            CFWriteStreamSetProperty(self.outputStream, kCFStreamPropertySSLSettings, settings as CFTypeRef)
        }
    }

    func closeStreams() {
        inputStream.removeFromRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        inputStream.close()
        inputStream.delegate = nil
    
        outputStream.removeFromRunLoop(NSRunLoop.currentRunLoop(), forMode:NSDefaultRunLoopMode);
        outputStream.close()
        outputStream.delegate = nil
        self.streamOpenCount = 0
    }
    
    func stream(aStream: NSStream, handleEvent eventCode: NSStreamEvent) {
        if (eventCode == NSStreamEvent.OpenCompleted) {
            self.streamOpenCount += 1
            assert(self.streamOpenCount <= 2);
            //Both streams are open therefore inform the delegate.
            if (self.streamOpenCount == 2) {
                self.delegate?.dataTransmittorDidOpenStreams()
                self.streamReader = StreamReader(stream: self.inputStream, delegate: self)
                self.streamWriter = StreamWriter(stream: self.outputStream, delegate: self)
            }
        } else if (eventCode == NSStreamEvent.HasSpaceAvailable) {
            print("hasSpaceAvailable")
            if let data = self.data {
                self.streamWriter?.writeData(data)
            }
        } else if (eventCode == NSStreamEvent.HasBytesAvailable) {
            print("hasBytesAvailable")
            if let stream = aStream as? NSInputStream {
                if streamReader == .None {
                    streamReader = StreamReader(stream: stream, delegate: self)
                }
                self.streamReader?.readStream()
            }
        } else if (eventCode == NSStreamEvent.ErrorOccurred) {
            print("Error occured")
            self.closeStreams()
            self.delegate?.dataTransmittor(self, receivedError: aStream.streamError)
        } else if (eventCode == NSStreamEvent.EndEncountered) {
            self.closeStreams()
            self.delegate?.dataTransmittorEnded()
        }
    }
    
    //MARK: StreamReaderDelegate
    func streamReader(streamReader: StreamReader, didStartReadingDataOfLength length: Int) {
        self.delegate?.dataTransmittorDidStartReadingData()
    }
    
    func streamReader(streamReader: StreamReader, didFinishReadingData data: NSData) {
        self.delegate?.dataTransmittorDidFinishReadingData(data)
        //completed reading so reset the stream reader for next data.
        self.streamReader?.reset()
    }

    func streamReader(streamReader: StreamReader, hadErrorReadingHeader error: NSError?) {
        print("Error reading header")
        self.closeStreams()
        self.delegate?.dataTransmittor(self, receivedError: error)
    }
    
    func streamReader(streamReader: StreamReader, hadErrorReadingBody error: NSError?) {
        print("Error reading body")
        self.closeStreams()
        self.delegate?.dataTransmittor(self, receivedError: error)
    }
    
    //MARK: StreamWriterDelegate
    func streamWriterDidStartWritingData() {
        self.delegate?.dataTransmittorDidStartWritingData()
    }
    
    func streamWriterDidFinishWritingData() {
        self.delegate?.dataTransmittorDidFinishWritingData()
    }
    
    func streamWriter(streamWriter: StreamWriter, hadErrorWritingHeader error: NSError?) {
        print("Error writing header")
        self.closeStreams()
        self.delegate?.dataTransmittor(self, receivedError: error)
    }
    
    func streamWriterHadFullCapacityHeaderError() {
        print("Error writing header due to full capacity")
    }
    
    func streamWriter(streamWriter: StreamWriter, hadErrorWritingBody error: NSError?) {
        print("Error writing body")
        self.closeStreams()
        self.delegate?.dataTransmittor(self, receivedError: error)
    }
    
    func streamWriterHadFullCapacityBodyError() {
        print("Error writing due to full capacity")
    }
    
    
    func streamWriterMissingDataError() {
        //try resending the data.
        guard let data = self.data else { return }
        streamWriter?.writeData(data)
    }
}