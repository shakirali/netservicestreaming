//
//  UIImage+ASImage.swift
//  NetServiceStreaming
//
//  Created by Shakir Ali on 12/01/2016.
//  Copyright © 2016 Shakir Ali. All rights reserved.
//

import UIKit

extension UIImage {

    func scaleToSize(size: CGSize) -> UIImage {
        let hasAlpha = false
        let scale: CGFloat = 0.0
        
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        self.drawInRect(CGRect(origin: CGPointZero, size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
}
